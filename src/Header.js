import React from "react"
import styled from 'styled-components'

import Home from './img/home.png'
import Gallery from './img/gallery.png'
import Mail from './img/mail.png'

import * as Scroll from 'react-scroll';
import { Link, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll'

const HeaderCon = styled.div`
    position: fixed;
    display: flex;
    justify-content: space-between;
    width: calc(100vw - 40px);
    padding: 20px;
    background: rgba(166, 119, 106, .25);

    img {
        height: 4vh;
    }

    .right {
        margin-left: 20px;
    }
`

function Header() {
  return (
    <HeaderCon>
        <div>
            <Link activeClass="active" to="home" spy={true} smooth={true} offset={0} duration={500}>
                <img src={Home} alt="home" />
            </Link>
        </div>
        <div>
            <Link activeClass="active" to="contact" className="right" spy={true} smooth={true} offset={0} duration={500}>
                <img src={Mail} alt="home" />
            </Link>
        </div>
    </HeaderCon>
  );
}
export default Header;