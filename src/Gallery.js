import React, { useState, useEffect } from "react";
import styled from "styled-components";
import SwiperCore, { Pagination } from 'swiper'
import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/scss'
import 'swiper/scss/pagination'
import * as Scroll from "react-scroll"
import { useAllPrismicDocumentsByType } from '@prismicio/react'

import Intro from './img/intro.jpeg'
import One from './img/1.jpeg'
import Two from './img/2.jpeg'
import Three from './img/3.jpeg'
import Four from './img/4.jpeg'
import Five from './img/5.jpeg'
import Six from './img/6.jpeg'
import Seven from './img/7.jpeg'
import Eight from './img/8.jpeg'
import Nine from './img/9.jpeg'
import Ten from './img/10.jpeg'
import Eleven from './img/11.jpeg'
import Twelve from './img/12.jpeg'
import Thrteen from './img/13.jpeg'
import Fourteen from './img/14.jpeg'

const Container = styled.section`
  padding: 100px 8vw 150px;
  display: grid;
  grid-template-columns: 1fr 1fr;
  column-gap: 4vw;
  width: 80vw;

  img {
    max-width: 40vw;
  }

  /* .gridPiece:nth-child(odd) {
    background-color: red;
  } */
`

const Gallery = () => {
  const Element = Scroll.Element
  SwiperCore.use([Pagination]);

  const [document] = useAllPrismicDocumentsByType('art')

  console.log(document);

  // const renderArtorks = () => {
  //   return document.map((art) => {
  //     return (
  //       <SwiperSlide key={art.id}>
  //         <img src={art.data.image1.url} className="hero-image" alt="img" />
  //         <div className="text">
  //           {art.data.artwork_name[0].text}
  //         </div>
  //       </SwiperSlide>
  //     )
  //   })
  // }

  return ( 
    <Element name="gallery">
      <Container>
        <div className="gridPiece"><img src={Intro} alt="one" /></div>
        <div className="gridPiece"></div>
        <div className="gridPiece"></div>
        <div className="gridPiece"><img src={Two} alt="one" /></div>
        <div className="gridPiece"><img src={One} alt="one" /></div>
        <div className="gridPiece"></div>
        <div className="gridPiece"></div>
        <div className="gridPiece"><img src={Five} alt="one" /></div>
        <div className="gridPiece"><img src={Six} alt="one" /></div>
        <div className="gridPiece"></div>
        <div className="gridPiece"></div>
        <div className="gridPiece"><img src={Seven} alt="one" /></div>
        <div className="gridPiece"><img src={Eight} alt="one" /></div>
        <div className="gridPiece"></div>
        <div className="gridPiece"></div>
        <div className="gridPiece"><img src={Twelve} alt="one" /></div>
        <div className="gridPiece"><img src={Thrteen} alt="one" /></div>
        <div className="gridPiece"></div>
        <div className="gridPiece"></div>
        <div className="gridPiece"><img src={Fourteen} alt="one" /></div>
        <div className="gridPiece"><img src={Three} alt="one" /></div>
        <div className="gridPiece"></div>
        <div className="gridPiece"></div>
        <div className="gridPiece"><img src={Nine} alt="one" /></div>
        <div className="gridPiece"><img src={Ten} alt="one" /></div>
        <div className="gridPiece"></div>
        <div className="gridPiece"></div>
        <div className="gridPiece"><img src={Eleven} alt="one" /></div>
        <div className="gridPiece"><img src={Four} alt="one" /></div>
      </Container>
    </Element>
  );
};

export default Gallery;