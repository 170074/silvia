import React from "react"
import styled from 'styled-components'

import Sunflower from './img/flower.jpeg'
import Lily from './img/lily2.jpeg'

import * as Scroll from "react-scroll"
import media from "./media"

const HeroCon = styled.div`
  height: 100%;
  min-height: 70vh;
  padding: 20px 0;
  display: flex;
  flex-direction: column;
  justify-content: center;
  background: #800020;
  background-image: url(${Lily});
  background-size: cover;
  background-position: center;
  height: 100vh;

  .container-text {
    /* background-image: url(${Sunflower});
    -webkit-text-fill-color: transparent;
    -webkit-background-clip: text;
    background-size: cover; */
    color:  #800020;
    padding-top: 10px;
    font-size: 15vw;
    text-transform: uppercase;
    font-family: sans-serif;
    font-weight: bold;
    background-position: center 50%;
  }

  ${media.laptop`
    .container-text {
      font-size: 10vw;
      text-align: left;
      margin-left: 50px;
    }
  `}
`

function Hero() {
  const Element = Scroll.Element

  return (
    <Element name="home">
      <HeroCon>
        <div className="container-text">
          Silvia
        </div>
        <div className="container-text">
          Fairhurst
        </div>
        <div className="container-text">
          Art
        </div>
      </HeroCon>
    </Element>
  );
}
export default Hero;