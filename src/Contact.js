import React from "react"
import styled from 'styled-components'

import * as Scroll from "react-scroll"
import media from "./media"

import Flower from './img/flower2.jpeg'

const ContactCon = styled.div`
  /* background: #080B11; */
  padding: 100px 8vw;
  color: white;
  text-align: left;

  h1 {
    font-size: 6vw;
    color: black;
  }

  p, a {
    font-size: 20px;
    color: black;
    text-decoration: none;
  }

  img {
    position: fixed;
    right: -50vw;
    top: 10vh;
    width: 200vw;
    z-index: -1;
    opacity: .4;
  }

  ${media.laptop`
    p {
      max-width: 60vw;
    }

    img {
      position: fixed;
      right: 0;
      top: 10vh;
      width: 50vw;
      z-index: -1;
      opacity: .4;
    }
  `}
`

function Contact() {
  const Element = Scroll.Element

  return (
    <Element name="contact">
      <ContactCon>
        <h1>Hi!</h1>
        <p>
          My name is Silvia Fairhurst and I am a local, South African artist from the East of Johannesburg. From a young age, I have been 
          inspired by the beauty of the world around me. My father was a sculptor from Italy, and passed on 
          his passion for creating beautiful pieces to me. With nature as my guide, I aim to produce inspiring 
          pieces that people can relate, and feel a spirritual connection to.<br/><br/>
          My artworks aim to provide a calming presence in any space, and envoke feelings of calm, serenity, and 
          passion. I am able to create a personal piece for any person or room, and enjoy spending time ideating 
          and envisioning an artwork that will inspire the viewer.
        </p>
        <br/>
        <br/>
        <p>For any inquiries, comissions or sales, please contact me on:</p><br/>
        <a href="mailto:silviafairhurst@gmail.com" target="_blank" rel="noreferrer">silviafairhurst@gmail.com</a><br/><br/>
        <a href="tel:0832267355" target="_blank" rel="noreferrer">083 226 7355</a><br/><br/>
        <a href="https://www.instagram.com/silviafairhurst" target="_blank" rel="noreferrer">@silviafairhurst</a>

        <img src={Flower} />
      </ContactCon>
    </Element>
  );
}
export default Contact;