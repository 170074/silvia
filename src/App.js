import logo from './logo.svg';
import './App.css';
import Hero from './Hero';
import Header from './Header';
import Contact from './Contact';
import Gallery from './Gallery';

import { PrismicProvider } from '@prismicio/react'
import { client } from './prismic'

function App() {
  return (
    <PrismicProvider client={client}>
      <div className="App">
        <Header />
        <Contact />
        {/* <Hero /> */}
        <Gallery />
      </div>
    </PrismicProvider>
  );
}

export default App;
